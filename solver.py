import random_board.py

def solve(board : [[]]) -> bool:
    
    find = find_empty(board)
    
    if not find:
        return True
    
    else:
        row, col = find

    for i in range(1,10):
        if valid(board, i, (row, col)):
            board[row][col] = i

            if solve(board):
                return True

            board[row][col] = 0

    return False


def valid(board : [[]], num : int, pos : int) -> bool:

    for i in range(len(board[0])):
        if board[pos[0]][i] == num and pos[1] != i:
            return False


    for i in range(len(board)):
        if board[i][pos[1]] == num and pos[0] != i:
            return False

  
    box_x = pos[1] // 3
    box_y = pos[0] // 3

    for i in range(box_y * 3, box_y * 3 + 3):
        for j in range(box_x * 3, box_x*3 + 3):
            if board[i][j] == num and (i,j) != pos:
                return False

    return True

def find_empty(board : [[]]) -> ():
    
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j] == 0:
                return (i, j)

    return None

def display(board : [[]]):
    
    for i in range(len(board)):
        if i % 3 == 0 and i != 0:
            print("\n- - - - - - - - - - - - -\n")

        for j in range(len(board[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end = "")

            if j == 8:
                print(board[i][j])
            else:
                print(str(board[i][j]) + " ", end = "")


def test(): 
  
    '''board = [
        [7, 8,'_', 4, '_', '_', 1, 2, '_'],
        [6, '_', '_', '_', 7, 5, '_', '_', 9],
        ['_', '_', '_', 6, '_', 1, '_', 7, 8],
        ['_', '_', 7, '_', 4, '_', 2, 6, '_'],
        ['_', '_', 1, '_', 5, '_', 9, 3, '_'],
        [9, '_', 4, '_', 6, '_','_', '_', 5],
        ['_', 7, '_', 3, '_', '_', '_', 1, 2],
        [1, 2, '_', '_', '0', 7, 4, '_', '_'],
        ['_', 4, 9, 2, '_', 6, '_', '_', 7]]
    '''

    board = make_board(3)    
    board = remove_numbers(board)   
    display(board)

    for i in board:
        for j in range(len(i)):
            if i[j] == '_':
                i[j] = 0  
    
    solve(board)
    display(board)

test()
