# Sudoku Solver

This is a Sudoku solver using the backtracking algorithm.
It includes a graphical GUI as well as a text based version.

In the text-based version, a random board will be generated and then the solved board will be printed out.

## Instructions

- Click a box and hit the number on your keybaord to pencil in a number. 
- To confirm that value press the ENTER key on that box. 
- To delete a pencil in you can click DEL. 
- Finally to solve the board press SPACE, sit back and watch the algorithm run.

## How to Run?

1. Text-based version

python3 solver.py

2. GUI 

pip3 install -r requirements.txt

python3 GUI.py

